{ pkgs, ... }: {
  roles =

    let scripts = import ./scripts.nix { inherit pkgs; };
    in {
      node = { pkgs, ... }: {
        networking.firewall.enable = false;
        # flemme
        boot.initrd.kernelModules =
          [ "squashfs" "loop" "overlay" "nfsv3" "igb" "ixgbe" ];

        environment.systemPackages = with pkgs; [
          htop
          scripts.mount_nfs_store
          scripts.mount_overlayfs
          scripts.activate
        ];
      };
    };
  testScript = ''
    foo.succeed("true")
  '';
}
