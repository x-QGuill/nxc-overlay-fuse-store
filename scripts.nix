{ pkgs, ... }: {
  mount_nfs_store = pkgs.writeScriptBin "mount_nfs_store" ''
    		username=$1
    		
    		 mkdir -p /nix/.server-ro-store
             mkdir -p /nix/.rw-store/work
             mkdir -p /nix/.rw-store/store

             mount -t nfs -o vers=3,nolock,ro,soft,retry=10 dignfs.grenoble.grid5000.fr:/export/home/$username/.nix/store/ /nix/.server-ro-store

        '';

  mount_overlayfs = pkgs.writeScriptBin "mount_overlayfs" ''
    mount -t overlay overlay -o lowerdir=/nix/.server-ro-store,upperdir=/nix/.rw-store/store,workdir=/nix/.rw-store/work /nix/store
  '';
  activate = pkgs.writeScriptBin "activate_conf" ''
    source $1/activate
  '';
}
